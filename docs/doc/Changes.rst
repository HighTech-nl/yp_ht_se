.. (C) 2020 ALbert Mietus, dropjes-licentie: copy & use, and betaal met dropjes naar nuttigheid

=======
Changes
=======

.. todo:: Write all improvement to DDS-Demonstrators and PET

This chapter list some changes to earlier edition/variations.

Coordination
============

* The team also has some more experienced developers, and testers.
* The training-repro (this) and the product-development repro(s) are split
* The (technical) ambition has become to improve/enhance existing (open-source) projects.

   - The ultimate PI (*Program Increment*) has become to get those changes merged to the master-repro
   - The team will learn to work on many **components**, and how to behave in a big “Train”

Technical
==========
* We use ``git`` now, instead of ``mercurial`` (``hg``).

